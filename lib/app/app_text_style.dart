import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppTextStyle {
  final BuildContext context;

  AppTextStyle._(this.context);

  factory AppTextStyle.of(BuildContext context) {
    return AppTextStyle._(context);
  }

  TextTheme buildTextTheme(bool primary) =>
      primary ? primaryTextTheme : textTheme;

  TextTheme get primaryTextTheme => Theme.of(context).primaryTextTheme;

  TextTheme get textTheme => Theme.of(context).textTheme;

  TextStyle button({
    Color color,
    bool primary = false,
  }) =>
      buildTextTheme(primary).button.copyWith(
            color: color ?? AppColors.accentColor,
            fontWeight: FontWeight.bold,
          );

  TextStyle caption({
    Color color,
    bool primary = false,
  }) {
    final result = buildTextTheme(primary).caption;

    return result.copyWith(
      color: color ?? result.color,
      fontWeight: FontWeight.bold,
    );
  }

  TextStyle body({
    Color color,
    bool primary = false,
  }) {
    final result = buildTextTheme(primary).bodyText2;

    return result.copyWith(
      color: color ?? result.color,
      fontWeight: FontWeight.bold,
    );
  }

  TextStyle subtitle({
    Color color,
    bool primary = false,
  }) {
    final result = buildTextTheme(primary).subtitle1;

    return result.copyWith(
      color: color ?? result.color,
      fontWeight: FontWeight.bold,
    );
  }

  TextStyle headline({
    Color color,
    bool primary = false,
  }) {
    final result = buildTextTheme(primary).headline6;

    return result.copyWith(
      color: color ?? result.color,
      fontWeight: FontWeight.bold,
    );
  }
}
