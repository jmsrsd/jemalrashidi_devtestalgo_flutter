import 'package:flutter/material.dart';
import '../page/meme/meme_page.dart';

import 'app_theme.dart';

class App extends StatelessWidget {
  final String title;

  App({
    this.title = '',
  });

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: title,
        theme: AppTheme().data,
        debugShowCheckedModeBanner: false,
        home: MemePage(),
      );
}
