import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app_colors.dart';

class AppTheme {
  ThemeData get data {
    final textThemeBuilder = GoogleFonts.schoolbellTextTheme;
    final result = ThemeData.light();

    return result.copyWith(
      accentColor: AppColors.accentColor,
      textTheme: textThemeBuilder(
        result.textTheme,
      ),
      primaryTextTheme: textThemeBuilder(
        result.primaryTextTheme,
      ),
      accentTextTheme: textThemeBuilder(
        result.accentTextTheme,
      ),
    );
  }
}
