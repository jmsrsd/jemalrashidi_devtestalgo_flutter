class AppApi {
  AppApi._();

  static const String hostUrl = 'https://api.imgflip.com';
  static const String getMemesUrl = '/get_memes';
}
