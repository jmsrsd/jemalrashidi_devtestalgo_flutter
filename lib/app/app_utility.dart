class AppUtility {
  AppUtility._();

  static DateTime convertDateString(String date) {
    final dateElements = date.split('-').map((e) => int.parse(e)).toList();

    return DateTime(
      dateElements[0],
      dateElements[1],
      dateElements[2],
    );
  }
}
