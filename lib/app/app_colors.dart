import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color primaryColor = const Color.fromRGBO(17, 152, 237, 1.0);
  static const Color secondaryColor = const Color.fromRGBO(104, 200, 255, 1.0);
  static const Color accentColor = const Color.fromRGBO(60, 184, 244, 1.0);

  static Color get cardColor => const Color.fromRGBO(223, 230, 230, 1.0);
  static Color get darkCardColor => const Color.fromRGBO(50, 50, 50, 1.0);
}
