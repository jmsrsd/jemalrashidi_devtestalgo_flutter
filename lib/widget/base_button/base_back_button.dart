import 'dart:async';

import 'package:flutter/material.dart';

import 'base_icon_button.dart';

class BaseBackButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BaseIconButton(
        iconData: Icons.arrow_back,
        onTap: () {
          Timer.run(() {
            Navigator.of(context).pop();
          });
        },
      );
}
