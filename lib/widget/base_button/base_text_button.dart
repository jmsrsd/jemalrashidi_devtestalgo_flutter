// @dart=2.9

import 'dart:async';

import '../../app/app_colors.dart';
import '../../app/app_text_style.dart';
import '../../widget/paper/paper.dart';
import 'package:flutter/material.dart';

class BaseTextButton extends StatelessWidget {
  final String data;
  final Color textColor;
  final FutureOr<void> Function() onTap;
  final FutureOr<void> Function() afterOnTap;
  final String onTapConfirmationMessage;
  final bool onTapConfirmationShown;
  final Color backgroundColor;
  final double elevation;
  final EdgeInsetsGeometry margin;

  BaseTextButton(
    this.data, {
    Color textColor,
    this.backgroundColor,
    this.onTap,
    this.afterOnTap,
    this.onTapConfirmationMessage,
    this.onTapConfirmationShown = false,
    this.elevation = 0.0,
    this.margin = EdgeInsets.zero,
  }) : this.textColor = textColor ?? AppColors.accentColor;

  @override
  Widget build(BuildContext context) => Paper(
        height: 40.0,
        margin: margin,
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        alignment: Alignment.center,
        onTap: onTap,
        onTapConfirmationMessage: onTapConfirmationMessage,
        afterOnTap: afterOnTap,
        onTapConfirmationShown: onTapConfirmationShown,
        color: backgroundColor,
        elevation: elevation,
        child: Text(
          data.toUpperCase(),
          style: AppTextStyle.of(context).button(
            color: textColor,
          ),
        ),
      );
}
