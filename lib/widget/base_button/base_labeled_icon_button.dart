// @dart=2.9

import 'dart:async';

import '../../app/app_colors.dart';
import '../../app/app_text_style.dart';
import '../../widget/paper/paper.dart';
import 'package:flutter/material.dart';

class BaseLabeledIconButton extends StatelessWidget {
  final IconData iconData;
  final double iconSize;
  final String labelData;

  final bool colorFlipped;

  final FutureOr<void> Function() onTap;
  final FutureOr<void> afterOnTap;
  final bool onTapConfirmationShown;
  final String onTapConfirmationMessage;

  final EdgeInsetsGeometry margin;
  final MainAxisSize mainAxisSize;

  BaseLabeledIconButton({
    @required this.iconData,
    @required this.labelData,
    this.iconSize,
    this.onTap,
    this.onTapConfirmationShown = false,
    this.afterOnTap,
    this.onTapConfirmationMessage,
    this.margin,
    this.colorFlipped = false,
    this.mainAxisSize = MainAxisSize.max,
  });

  @override
  Widget build(BuildContext context) => Paper(
        height: 40.0,
        padding: EdgeInsets.all(8.0),
        margin: margin,
        onTap: onTap,
        afterOnTap: afterOnTap,
        onTapConfirmationShown: onTapConfirmationShown,
        onTapConfirmationMessage: onTapConfirmationMessage,
        color: colorFlipped ? AppColors.accentColor : Colors.transparent,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: mainAxisSize,
          children: [
            if (iconData != null)
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8.0),
                child: Icon(
                  iconData,
                  size: iconSize ?? 24.0,
                  color: colorFlipped
                      ? AppColors.cardColor
                      : AppColors.accentColor,
                ),
              ),
            if (labelData != null)
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  labelData.toUpperCase(),
                  style: AppTextStyle.of(context).button(
                    color: colorFlipped
                        ? AppColors.cardColor
                        : AppColors.accentColor,
                  ),
                ),
              ),
          ],
        ),
      );
}
