import 'dart:async';

import 'package:flutter/material.dart';

import 'base_icon_button.dart';

class BaseCloseButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BaseIconButton(
        iconData: Icons.close,
        onTap: () {
          Timer.run(() {
            Navigator.of(context).pop();
          });
        },
      );
}
