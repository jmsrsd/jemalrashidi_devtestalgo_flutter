import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as urlLauncher;

import 'base_icon_button.dart';

class BaseIconUrlButton extends StatelessWidget {
  final IconData iconData;
  final String url;
  final EdgeInsetsGeometry margin;

  BaseIconUrlButton(
    this.iconData,
    this.url, {
    this.margin = EdgeInsets.zero,
  });

  @override
  Widget build(BuildContext context) => BaseIconButton(
        margin: margin,
        iconData: iconData,
        onTap: () async {
          if (await urlLauncher.canLaunch(url))
            await Future.wait([
              urlLauncher.launch(url),
              Future.delayed(Duration(seconds: 3)),
            ]);
        },
      );
}
