// @dart=2.9

import 'dart:async';

import 'package:flutter/material.dart';

import '../../app/app_colors.dart';
import '../paper/paper.dart';

class BaseIconButton extends StatelessWidget {
  final IconData iconData;
  final double iconSize;
  final FutureOr<void> Function() onTap;
  final FutureOr<void> Function() afterOnTap;
  final bool onTapConfirmationShown;
  final String onTapConfirmationMessage;
  final EdgeInsetsGeometry margin;
  final bool colorFlipped;

  BaseIconButton({
    this.onTap,
    this.afterOnTap,
    this.iconData,
    this.iconSize = 24.0,
    this.onTapConfirmationShown = false,
    this.onTapConfirmationMessage,
    this.margin = EdgeInsets.zero,
    this.colorFlipped = false,
  });

  Color get iconColor => onTap == null && afterOnTap == null
      ? Colors.transparent
      : (colorFlipped ? AppColors.cardColor : AppColors.accentColor);

  Color get backgroundColor => onTap == null && afterOnTap == null
      ? Colors.transparent
      : (colorFlipped ? AppColors.accentColor : Colors.transparent);

  @override
  Widget build(BuildContext context) => Paper(
        width: 40.0,
        height: 40.0,
        margin: margin,
        alignment: Alignment.center,
        color: backgroundColor,
        onTapConfirmationShown: onTapConfirmationShown,
        onTapConfirmationMessage: onTapConfirmationMessage,
        onTap: onTap,
        afterOnTap: afterOnTap,
        child: iconData == null
            ? SizedBox()
            : Icon(
                iconData,
                size: iconSize,
                color: iconColor,
              ),
      );
}
