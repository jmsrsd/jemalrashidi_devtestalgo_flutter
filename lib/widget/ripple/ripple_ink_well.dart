// @dart=2.9

import 'package:flutter/material.dart';

class RippleInkWell extends StatelessWidget {
  final void Function(BuildContext context) onTapBuilder;

  RippleInkWell({
    @required this.onTapBuilder,
  });

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () {
          if (onTapBuilder != null) onTapBuilder(context);
        },
      );
}
