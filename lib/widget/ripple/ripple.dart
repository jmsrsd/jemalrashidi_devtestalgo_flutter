// @dart=2.9

import 'dart:async';

import '../../widget/future_dialog/future_dialog.dart';
import '../../widget/ripple/ripple_ink_well.dart';
import 'package:flutter/material.dart';

class Ripple extends StatelessWidget {
  final FutureOr<void> Function() onTap;
  final FutureOr<void> Function() afterOnTap;
  final bool onTapConfirmationShown;
  final String onTapConfirmationMessage;

  Ripple({
    this.onTap,
    this.afterOnTap,
    this.onTapConfirmationShown,
    this.onTapConfirmationMessage,
  });

  FutureDialog get futureDialogInstance => FutureDialog(
        confirmation: onTapConfirmationShown ?? false,
        confirmationMessage: onTapConfirmationMessage ?? '',
        futureBuilder: onTap,
        afterFutureBuilder: afterOnTap,
      );

  void onTapBuilder(BuildContext context) {
    FutureDialog.show(
      context: context,
      instance: futureDialogInstance,
    );
  }

  @override
  Widget build(BuildContext context) => onTap == null && afterOnTap == null
      ? SizedBox()
      : Positioned.fill(
          child: RippleInkWell(
            onTapBuilder: onTapBuilder,
          ),
        );
}
