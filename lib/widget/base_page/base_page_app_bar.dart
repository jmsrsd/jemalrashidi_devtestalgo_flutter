// @dart=2.9

import '../../app/app_colors.dart';
import '../../widget/paper/paper.dart';
import 'package:flutter/material.dart';

import 'base_page_app_bar_button_row.dart';
import 'base_page_app_bar_title_view.dart';

class BasePageAppBar extends StatelessWidget {
  static const double defaultHeight = 56.0;

  final Widget navigationButton;
  final Widget title;
  final List<Widget> actionButtons;
  final bool darkMode;

  BasePageAppBar({
    this.navigationButton,
    this.title,
    this.actionButtons,
    this.darkMode = false,
  });

  @override
  Widget build(BuildContext context) => Paper(
        height: defaultHeight,
        color: darkMode ? AppColors.darkCardColor : AppColors.cardColor,
        rounded: false,
        elevation: darkMode ? 0.0 : 4.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            navigationButton == null
                ? SizedBox(width: 8.0)
                : BasePageAppBarButtonRow(
                    children: [
                      AspectRatio(
                        aspectRatio: 1.0,
                        child: Center(
                          child: navigationButton,
                        ),
                      ),
                    ],
                  ),
            Expanded(
              child: BasePageAppBarTitleView(
                child: title,
              ),
            ),
            BasePageAppBarButtonRow(
              children: (actionButtons ?? const [])
                  .map(
                    (e) => Container(
                      margin: EdgeInsets.only(left: 8.0),
                      child: e,
                    ),
                  )
                  .toList(),
            ),
          ],
        ),
      );
}
