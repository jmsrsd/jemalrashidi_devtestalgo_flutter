// @dart=2.9

import '../../app/app_colors.dart';
import '../../widget/base_button/base_icon_button.dart';
import '../../widget/paper/paper.dart';
import 'package:flutter/material.dart';

import 'base_page.dart';
import 'base_page_title.dart';

class BaseAdminPage extends StatelessWidget {
  final bool logoutButtonShown;
  final List<Widget> actionButtons;
  final List<Widget> sideNavigationChildren;
  final Widget body;
  final bool bodyScrollable;

  BaseAdminPage({
    this.logoutButtonShown = false,
    this.actionButtons,
    this.sideNavigationChildren,
    this.body,
    this.bodyScrollable = true,
  });

  @override
  Widget build(BuildContext context) => BasePage(
        withStatusBar: false,
        navigationButton: logoutButtonShown == false
            ? null
            : BaseIconButton(
                iconData: Icons.logout,
                onTap: () async {},
              ),
        actionButtons: actionButtons ?? [],
        title: BasePageTitle(
          'Admin Dashboard E-Paper Blok-A',
          withMargin: true,
        ),
        body: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Paper(
              width: 240.0,
              height: double.maxFinite,
              elevation: 3.0,
              color: AppColors.cardColor,
              rounded: false,
              alignment: Alignment.topLeft,
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                  vertical: 16.0,
                  horizontal: 8.0,
                ),
                child: Column(
                  children: (sideNavigationChildren ?? [])
                      .map(
                        (e) => Container(
                          margin: EdgeInsets.only(bottom: 8.0),
                          child: e,
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
            Expanded(
              child: bodyScrollable == false
                  ? body
                  : Align(
                      alignment: Alignment.topLeft,
                      child: SingleChildScrollView(
                        padding: EdgeInsets.all(16.0),
                        child: body,
                      ),
                    ),
            ),
          ],
        ),
      );
}
