// @dart=2.9

import '../../app/app_colors.dart';
import '../../widget/paper/paper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'base_page_app_bar.dart';
import 'base_page_bottom_navigation.dart';

class BasePage extends StatelessWidget {
  static const double appBarHeight = 56.0;

  final Widget navigationButton;
  final List<Widget> bottomNavigationButtons;
  final double bottomNavigationHeight;
  final Widget title;
  final List<Widget> actionButtons;
  final Widget body;
  final bool darkMode;
  final bool appBarEnabled;
  final bool bottomNavigationEnabled;
  final bool withStatusBar;
  final bool resizeToAvoidBottomInset;

  BasePage({
    this.navigationButton,
    this.bottomNavigationButtons = const [],
    this.title,
    this.actionButtons,
    this.body,
    this.darkMode = false,
    this.appBarEnabled = true,
    this.bottomNavigationEnabled = true,
    double bottomNavigationHeight,
    this.withStatusBar = true,
    this.resizeToAvoidBottomInset = true,
  }) : this.bottomNavigationHeight =
            bottomNavigationEnabled ? (bottomNavigationHeight ?? 56.0) : 0.0;

  SystemUiOverlayStyle get systemUiOverlayStyle =>
      darkMode ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark;

  Color get defaultBackgroundColor =>
      darkMode ? AppColors.darkCardColor : AppColors.cardColor;

  Color get defaultStatusBarColor =>
      darkMode ? AppColors.darkCardColor : AppColors.cardColor;

  double get statusBarHeight => withStatusBar == false ? 0.0 : 24.0;

  @override
  Widget build(BuildContext context) {
    return Paper(
      color: defaultBackgroundColor,
      rounded: false,
      child: AnnotatedRegion(
        value: systemUiOverlayStyle.copyWith(
          statusBarColor: Colors.transparent,
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomInset: resizeToAvoidBottomInset,
          body: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: statusBarHeight + (appBarEnabled ? appBarHeight : 0.0),
                  bottom: bottomNavigationHeight,
                ),
                child: body ?? SizedBox(),
              ),
              if (bottomNavigationEnabled)
                Align(
                  alignment: Alignment.bottomCenter,
                  child: BasePageBottomNavigation(
                    darkMode: darkMode,
                    height: bottomNavigationHeight,
                    children: bottomNavigationButtons,
                  ),
                ),
              if (appBarEnabled)
                Padding(
                  padding: EdgeInsets.only(
                    top: statusBarHeight,
                  ),
                  child: BasePageAppBar(
                    navigationButton: navigationButton,
                    title: title,
                    actionButtons: actionButtons,
                    darkMode: darkMode,
                  ),
                ),
              Container(
                height: statusBarHeight,
                color: defaultStatusBarColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
