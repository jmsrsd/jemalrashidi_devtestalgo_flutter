import '../../app/app_text_style.dart';
import 'package:flutter/material.dart';

class BasePageTitle extends StatelessWidget {
  final String titleData;
  final bool withMargin;

  BasePageTitle(
    this.titleData, {
    this.withMargin = false,
  });

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.symmetric(
          horizontal: withMargin ? 8.0 : 0.0,
        ),
        child: Text(
          titleData,
          style: AppTextStyle.of(context).headline(),
        ),
      );
}
