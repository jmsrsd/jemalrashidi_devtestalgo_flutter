// @dart=2.9

import 'package:flutter/material.dart';

class BasePageAppBarTitleView extends StatelessWidget {
  final Widget child;

  BasePageAppBarTitleView({
    @required this.child,
  });

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        padding: EdgeInsets.zero,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: child ?? SizedBox(),
      );
}
