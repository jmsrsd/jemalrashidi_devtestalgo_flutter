// @dart=2.9

import '../../app/app_colors.dart';
import '../../widget/paper/paper.dart';
import 'package:flutter/material.dart';

class BasePageBottomNavigation extends StatelessWidget {
  static const double defaultHeight = 56.0;

  final List<Widget> children;
  final bool darkMode;
  final double height;

  BasePageBottomNavigation({
    this.children = const [],
    this.darkMode = false,
    this.height,
  });

  @override
  Widget build(BuildContext context) => Paper(
        color: darkMode ? AppColors.darkCardColor : AppColors.cardColor,
        rounded: false,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Divider(
              height: 1.0,
              thickness: 1.0,
            ),
            Paper(
              width: double.maxFinite,
              height: this.height ?? defaultHeight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: children.take(5).toList(),
              ),
            ),
          ],
        ),
      );
}
