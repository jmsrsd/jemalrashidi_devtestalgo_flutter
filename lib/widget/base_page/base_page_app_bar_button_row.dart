import 'package:flutter/material.dart';

class BasePageAppBarButtonRow extends StatelessWidget {
  final List<Widget> children;

  BasePageAppBarButtonRow({
    this.children = const [],
  });

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.only(right: 8.0),
        child: SingleChildScrollView(
          padding: EdgeInsets.zero,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.horizontal,
          reverse: true,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: children,
          ),
        ),
      );
}
