import 'dart:async';

import 'package:flutter/material.dart';
import 'package:jemalrashidi_devtestalgo_flutter/app/app_text_style.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_button/base_icon_button.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/paper/paper.dart';

class BasePageNavigationAppBar extends StatelessWidget {
  final FutureOr<void> Function() onBackButtonTap;
  final FutureOr<void> Function() onForwardButtonTap;
  final IconData backIconData;
  final IconData forwardIconData;
  final String titleData;

  BasePageNavigationAppBar({
    this.titleData,
    this.backIconData,
    this.onBackButtonTap,
    this.forwardIconData,
    this.onForwardButtonTap,
  });

  @override
  Widget build(BuildContext context) => Paper(
        rounded: false,
        height: 56.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AspectRatio(
              aspectRatio: 1.0,
              child: Center(
                child: BaseIconButton(
                  iconData: backIconData ?? Icons.arrow_back,
                  afterOnTap: onBackButtonTap,
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: Text(
                  titleData ?? '',
                  style: AppTextStyle.of(context).headline(),
                ),
              ),
            ),
            AspectRatio(
              aspectRatio: 1.0,
              child: Center(
                child: BaseIconButton(
                  iconData: forwardIconData ?? Icons.arrow_forward,
                  afterOnTap: onForwardButtonTap,
                ),
              ),
            ),
          ],
        ),
      );
}
