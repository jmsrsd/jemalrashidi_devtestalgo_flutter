// @dart=2.9

import 'dart:async';

import '../../widget/ripple/ripple.dart';
import 'package:flutter/material.dart';

import 'paper_border_radius.dart';

class Paper extends StatelessWidget {
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;
  final AlignmentGeometry alignment;
  final bool rounded;
  final Color color;
  final double elevation;
  final double width;
  final double height;
  final Widget child;
  final FutureOr<void> Function() onTap;
  final FutureOr<void> Function() afterOnTap;
  final bool onTapConfirmationShown;
  final String onTapConfirmationMessage;

  Paper({
    this.margin = EdgeInsets.zero,
    this.padding = EdgeInsets.zero,
    this.alignment,
    this.rounded = true,
    this.color,
    this.elevation = 0.0,
    this.width,
    this.height,
    this.child,
    this.onTap,
    this.afterOnTap,
    this.onTapConfirmationShown = false,
    this.onTapConfirmationMessage,
  });

  @override
  Widget build(BuildContext context) => Container(
        margin: margin,
        child: Material(
          color: color ?? Colors.transparent,
          elevation: elevation,
          clipBehavior: Clip.antiAlias,
          borderRadius: PaperBorderRadius.rounded(rounded),
          child: Stack(
            children: [
              SizedBox(
                width: width,
                height: height,
                child: Container(
                  padding: padding,
                  alignment: alignment,
                  child: child ?? SizedBox(),
                ),
              ),
              Ripple(
                onTapConfirmationShown: onTapConfirmationShown,
                afterOnTap: afterOnTap,
                onTapConfirmationMessage: onTapConfirmationMessage,
                onTap: onTap,
              ),
            ],
          ),
        ),
      );
}
