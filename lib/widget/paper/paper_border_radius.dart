import 'package:flutter/material.dart';

class PaperBorderRadius {
  PaperBorderRadius._();

  static BorderRadius rounded(bool value) => value
      ? BorderRadius.only(
          topRight: Radius.circular(8.0),
          topLeft: Radius.circular(8.0),
          bottomRight: Radius.circular(8.0),
          bottomLeft: Radius.circular(8.0),
        )
      : BorderRadius.zero;
}
