// @dart=2.9

import '../../../app/app_text_style.dart';
import 'package:flutter/material.dart';

class ConfirmingFutureDialogSubmitAction extends StatelessWidget {
  final void Function() onTap;

  ConfirmingFutureDialogSubmitAction({
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) => Container(
        height: 40.0,
        child: FlatButton(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          onPressed: onTap ?? () {},
          child: Text(
            'YES',
            style: AppTextStyle.of(context).button(),
          ),
        ),
      );
}
