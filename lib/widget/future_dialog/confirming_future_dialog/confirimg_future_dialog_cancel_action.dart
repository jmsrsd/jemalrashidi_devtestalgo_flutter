import '../../../app/app_text_style.dart';
import 'package:flutter/material.dart';

class ConfirmingFutureDialogCancelAction extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        height: 40.0,
        child: FlatButton(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'NO',
            style: AppTextStyle.of(context).button(),
          ),
        ),
      );
}
