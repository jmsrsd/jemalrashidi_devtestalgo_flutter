// @dart=2.9

import '../../../widget/paper/paper.dart';
import 'package:flutter/material.dart';

import 'confirimg_future_dialog_cancel_action.dart';
import 'confirimg_future_dialog_submit_action.dart';

class ConfirmingFutureDialog extends StatelessWidget {
  final void Function() onSubmit;
  final Widget child;

  ConfirmingFutureDialog({
    @required this.onSubmit,
    @required this.child,
  });

  List<Widget> get actions => [
        ConfirmingFutureDialogCancelAction(),
        ConfirmingFutureDialogSubmitAction(
          onTap: onSubmit,
        ),
      ];

  @override
  Widget build(BuildContext context) => Paper(
        child: AlertDialog(
          title: Text('Confirmation'),
          content: child ?? SizedBox(),
          actions: actions,
        ),
      );
}
