import 'dart:async';

import 'package:async/async.dart';
import '../../app/app_text_style.dart';
import 'package:flutter/material.dart';

import 'confirming_future_dialog/confirming_future_dialog.dart';
import 'progressing_future_dialog.dart';

class FutureDialog extends StatefulWidget {
  final bool confirmation;
  final String confirmationMessage;
  final FutureOr<void> Function() futureBuilder;
  final FutureOr<void> Function() afterFutureBuilder;

  FutureDialog({
    this.confirmation,
    this.confirmationMessage,
    this.futureBuilder,
    this.afterFutureBuilder,
  });

  static void show({
    @required BuildContext context,
    @required FutureDialog instance,
  }) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => instance,
    );
  }

  @override
  _State createState() => _State();
}

class _State extends State<FutureDialog> {
  CancelableOperation cancelableOperation;
  bool confirmed;

  void initializeCancelableOperation() {
    if (confirmed == false) return;

    cancelableOperation = CancelableOperation.fromFuture(Future(
      widget.futureBuilder ?? () async {},
    )).then((_) async {
      Navigator.of(context).pop();
      if (widget.afterFutureBuilder != null) await widget.afterFutureBuilder();
    });
  }

  @override
  void initState() {
    super.initState();

    confirmed = (widget.confirmation ?? false) == false;

    initializeCancelableOperation();
  }

  @override
  Widget build(BuildContext context) => confirmed
      ? ProgressingFutureDialog(
          cancelableOperation: cancelableOperation,
        )
      : ConfirmingFutureDialog(
          onSubmit: () {
            setState(() {
              confirmed = true;
              initializeCancelableOperation();
            });
          },
          child: Text(
            widget.confirmationMessage ?? '',
            style: AppTextStyle.of(context).body(),
          ),
        );
}
