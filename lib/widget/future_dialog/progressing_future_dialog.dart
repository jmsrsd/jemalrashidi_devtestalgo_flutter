// @dart=2.9

import 'package:async/async.dart';
import '../../widget/paper/paper.dart';

import 'package:flutter/material.dart';

class ProgressingFutureDialog extends StatelessWidget {
  final CancelableOperation cancelableOperation;

  ProgressingFutureDialog({
    @required this.cancelableOperation,
  });

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          if (cancelableOperation != null) await cancelableOperation.cancel();
          return false;
        },
        child: Paper(
          child: AlertDialog(
            title: Text('Berproses'),
            content: LinearProgressIndicator(),
          ),
        ),
      );
}
