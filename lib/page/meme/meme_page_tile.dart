import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jemalrashidi_devtestalgo_flutter/app/app_colors.dart';
import 'package:jemalrashidi_devtestalgo_flutter/page/meme_detail/meme_detail_page.dart';
import '../../widget/paper/paper.dart';
import 'meme.dart';

class MemePageTile extends StatelessWidget {
  final Meme meme;

  MemePageTile({
    @required this.meme,
  });

  @override
  Widget build(BuildContext context) => Paper(
        width: double.maxFinite,
        height: double.maxFinite,
        margin: EdgeInsets.only(
          right: 8.0,
          bottom: 8.0,
        ),
        child: Stack(
          children: [
            CachedNetworkImage(
              imageUrl: meme.url,
              placeholder: (context, url) => Center(
                child: SizedBox(
                  width: 24.0,
                  height: 24.0,
                  child: CircularProgressIndicator(),
                ),
              ),
              errorWidget: (context, url, error) => Center(
                child: Icon(
                  Icons.image,
                  color: AppColors.accentColor,
                ),
              ),
              alignment: Alignment.topCenter,
              fit: BoxFit.cover,
              width: double.maxFinite,
              height: double.maxFinite,
            ),
            Paper(
              rounded: false,
              width: double.maxFinite,
              height: double.maxFinite,
              afterOnTap: () async {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => MemeDetailPage(
                      meme: meme,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      );
}
