class Meme {
  final String id;
  final String name;
  final String url;
  final int width;
  final int height;
  final int boxCount;

  Meme._({
    this.id,
    this.name,
    this.url,
    this.width,
    this.height,
    this.boxCount,
  });

  factory Meme({
    String id,
    String name,
    String url,
    int width,
    int height,
    int boxCount,
  }) {
    return Meme._(
      id: id ?? '',
      name: name ?? '',
      url: url ?? '',
      width: width ?? 0,
      height: height ?? 0,
      boxCount: boxCount ?? 0,
    );
  }

  factory Meme.fromJson(Map<String, dynamic> json) {
    return Meme(
      id: json["id"] as String,
      name: json["name"] as String,
      url: json["url"] as String,
      width: json["width"] as int,
      height: json["height"] as int,
      boxCount: json["box_count"] as int,
    );
  }
}
