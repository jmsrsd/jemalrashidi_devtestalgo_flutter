import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jemalrashidi_devtestalgo_flutter/app/app_api.dart';
import 'package:jemalrashidi_devtestalgo_flutter/page/meme/meme_page_tile.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_page/base_page_navigation_app_bar.dart';

import '../../widget/base_page/base_page.dart';
import 'meme.dart';

class MemePage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<MemePage> {
  @override
  Widget build(BuildContext context) => BasePage(
        appBarEnabled: false,
        bottomNavigationEnabled: false,
        body: Column(
          children: [
            BasePageNavigationAppBar(
              titleData: 'MimGenerator',
            ),
            Expanded(
              child: FutureBuilder<List<Meme>>(
                future: Future(() async {
                  final response = await http.get(
                    AppApi.hostUrl + AppApi.getMemesUrl,
                  );

                  final body = jsonDecode(response.body);
                  final data = body['data'] as Map<String, dynamic>;
                  final memes = data['memes'] as List<dynamic>;

                  return memes
                      .map((e) => Meme.fromJson(e as Map<String, dynamic>))
                      .toList();
                }),
                builder: (context, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done)
                    return Center(
                      child: CircularProgressIndicator(),
                    );

                  final memes = snapshot.data ?? <Meme>[];

                  return RefreshIndicator(
                    onRefresh: () async {
                      setState(() {});
                    },
                    child: GridView.count(
                      crossAxisCount: 3,
                      childAspectRatio: 1.0,
                      padding: EdgeInsets.all(8.0).copyWith(right: 0.0),
                      physics: const AlwaysScrollableScrollPhysics(),
                      children: memes
                          .map(
                            (e) => MemePageTile(meme: e),
                          )
                          .toList(),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      );
}
