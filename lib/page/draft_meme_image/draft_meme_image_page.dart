import 'dart:io';
import 'dart:typed_data';

import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:flutter/material.dart';
import 'package:jemalrashidi_devtestalgo_flutter/app/app_colors.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_button/base_labeled_icon_button.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_button/base_text_button.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_page/base_page.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_page/base_page_navigation_app_bar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:path/path.dart' as path;

class DraftMemeImagePage extends StatefulWidget {
  final Uint8List draftMemeImage;

  DraftMemeImagePage({
    @required this.draftMemeImage,
  });

  @override
  _State createState() => _State();
}

class _State extends State<DraftMemeImagePage> {
  @override
  Widget build(BuildContext context) => BasePage(
        appBarEnabled: false,
        bottomNavigationButtons: [
          BaseTextButton(
            'Simpan',
            margin: EdgeInsets.only(
              left: 8.0,
              right: 4.0,
            ),
            backgroundColor: AppColors.accentColor,
            textColor: AppColors.cardColor,
            afterOnTap: () async {
              final directoryPath = await FilesystemPicker.open(
                  context: context,
                  title: 'Simpan mim',
                  pickText: 'Simpan mim di sini',
                  fsType: FilesystemType.file,
                  fileTileSelectMode: FileTileSelectMode.wholeTile,
                  rootDirectory: Directory('/storage/emulated/0/'),
                  requestPermission: () async {
                    return await Permission.storage.request().isGranted;
                  });

              if (directoryPath == null) return;

              print(directoryPath);

              // final file = File(
              //   path.join(
              //     directoryPath,
              //     'mim_${DateTime.now().microsecondsSinceEpoch}.png',
              //   ),
              // );

              // if (widget.draftMemeImage == null) return;

              // await file.writeAsBytes(widget.draftMemeImage);
            },
          ),
          BaseTextButton(
            'Share',
            margin: EdgeInsets.only(
              left: 4.0,
              right: 8.0,
            ),
            backgroundColor: AppColors.accentColor,
            textColor: AppColors.cardColor,
            afterOnTap: () async {},
          ),
        ].map((e) => Expanded(child: e)).toList(),
        body: Column(
          children: [
            BasePageNavigationAppBar(
              titleData: 'MimGenerator',
              onBackButtonTap: () async {
                Navigator.of(context).pop();
              },
            ),
            Expanded(
              child: Center(
                child: Image.memory(
                  widget.draftMemeImage ?? kTransparentImage,
                ),
              ),
            ),
          ],
        ),
      );
}
