import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:jemalrashidi_devtestalgo_flutter/app/app_text_style.dart';
import 'package:screenshot/screenshot.dart';
import 'package:transparent_image/transparent_image.dart';

class MemeDetailPageOverlay extends StatefulWidget {
  final String text;
  final Uint8List logo;

  MemeDetailPageOverlay({
    this.text,
    this.logo,
  });

  @override
  _State createState() => _State();
}

class _State extends State<MemeDetailPageOverlay> {
  @override
  Widget build(BuildContext context) => Positioned.fill(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                height: 56.0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    AspectRatio(
                      aspectRatio: 1.0,
                      child: Image.memory(widget.logo ?? kTransparentImage),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16.0),
                      child: Text(
                        widget.text ?? '',
                        style: AppTextStyle.of(context).headline(),
                        maxLines: 1,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Container(
                height: 56.0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      widget.text ?? '',
                      style: AppTextStyle.of(context).headline(),
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
