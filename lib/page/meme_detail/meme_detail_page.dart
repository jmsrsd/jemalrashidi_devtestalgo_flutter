import 'dart:typed_data';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jemalrashidi_devtestalgo_flutter/app/app_colors.dart';
import 'package:jemalrashidi_devtestalgo_flutter/app/app_text_style.dart';
import 'package:jemalrashidi_devtestalgo_flutter/page/draft_meme_image/draft_meme_image_page.dart';
import 'package:jemalrashidi_devtestalgo_flutter/page/meme/meme.dart';
import 'package:jemalrashidi_devtestalgo_flutter/page/meme_detail/meme_detail_page_overlay.dart';
import 'package:jemalrashidi_devtestalgo_flutter/page/text_editor/text_editor_page.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_button/base_labeled_icon_button.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_page/base_page.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_page/base_page_navigation_app_bar.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/paper/paper.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:screenshot/screenshot.dart';

class MemeDetailPage extends StatefulWidget {
  final Meme meme;

  MemeDetailPage({
    @required this.meme,
  });

  @override
  _State createState() => _State();
}

class _State extends State<MemeDetailPage> {
  String overlayText;
  Uint8List overlayLogo;
  final ScreenshotController screenshotController = ScreenshotController();

  @override
  Widget build(BuildContext context) => BasePage(
        appBarEnabled: false,
        bottomNavigationButtons: [
          BaseLabeledIconButton(
            margin: EdgeInsets.only(
              left: 8.0,
              right: 4.0,
            ),
            iconData: Icons.image,
            labelData: 'Add Logo',
            colorFlipped: true,
            mainAxisSize: MainAxisSize.max,
            afterOnTap: () async {
              final pickedAssets = await MultiImagePicker.pickImages(
                maxImages: 1,
              );

              if (pickedAssets == null) return;
              if (pickedAssets.isEmpty) return;

              final pickedImage =
                  (await pickedAssets.first.getByteData()).buffer.asUint8List();

              setState(() {
                overlayLogo = pickedImage;
              });
            },
          ),
          BaseLabeledIconButton(
            margin: EdgeInsets.only(
              left: 4.0,
              right: 8.0,
            ),
            iconData: Icons.text_fields,
            labelData: 'Add Text',
            colorFlipped: true,
            mainAxisSize: MainAxisSize.max,
            afterOnTap: () async {
              final text = await Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => TextEditorPage(),
                ),
              );

              if (text == null) return;

              setState(() {
                overlayText = text;
              });
            },
          ),
        ].map((e) => Expanded(child: e)).toList(),
        body: Column(
          children: [
            BasePageNavigationAppBar(
              titleData: 'MimGenerator',
              onBackButtonTap: () async {
                Navigator.of(context).pop();
              },
              forwardIconData: Icons.check,
              onForwardButtonTap: () async {
                final imageFile = await screenshotController.capture();

                if (imageFile == null) return;

                final image = await imageFile.readAsBytes();

                if (image == null) return;

                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => DraftMemeImagePage(
                      draftMemeImage: image,
                    ),
                  ),
                );
              },
            ),
            Expanded(
              child: Center(
                child: Paper(
                  rounded: false,
                  margin: EdgeInsets.all(16.0),
                  child: CachedNetworkImage(
                    imageUrl: widget.meme.url,
                    imageBuilder: (context, imageProvider) => Image(
                      image: imageProvider,
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      frameBuilder: (
                        context,
                        child,
                        frame,
                        wasSynchronouslyLoaded,
                      ) {
                        return Screenshot(
                          controller: screenshotController,
                          child: Stack(
                            children: [
                              child,
                              MemeDetailPageOverlay(
                                text: overlayText,
                                logo: overlayLogo,
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    placeholder: (context, url) => Center(
                      child: SizedBox(
                        width: 24.0,
                        height: 24.0,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                    errorWidget: (context, url, error) => Center(
                      child: Icon(
                        Icons.image,
                        color: AppColors.accentColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
