import 'package:flutter/material.dart';
import 'package:jemalrashidi_devtestalgo_flutter/app/app_text_style.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_page/base_page.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/base_page/base_page_navigation_app_bar.dart';
import 'package:jemalrashidi_devtestalgo_flutter/widget/future_dialog/future_dialog.dart';

class TextEditorPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<TextEditorPage> {
  final TextEditingController controller = TextEditingController();

  void showCancelDialog() {
    FutureDialog.show(
      context: context,
      instance: FutureDialog(
        confirmation: true,
        confirmationMessage: 'Cancel Add Text?',
        afterFutureBuilder: () async {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  void showSumbitDialog() {
    FutureDialog.show(
      context: context,
      instance: FutureDialog(
        confirmation: true,
        confirmationMessage: 'Submit text?',
        afterFutureBuilder: () async {
          Navigator.of(context).pop(controller.text);
        },
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          showCancelDialog();

          return false;
        },
        child: BasePage(
          appBarEnabled: false,
          bottomNavigationEnabled: false,
          body: Column(
            children: [
              BasePageNavigationAppBar(
                titleData: 'MimGenerator',
                backIconData: Icons.close,
                onBackButtonTap: () async {
                  showCancelDialog();
                },
                forwardIconData: Icons.check,
                onForwardButtonTap: () async {
                  showSumbitDialog();
                },
              ),
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(16.0),
                  child: TextField(
                    maxLines: null,
                    controller: controller,
                    style: AppTextStyle.of(context).body(),
                    decoration: InputDecoration(
                      filled: true,
                      hintText: 'Text',
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
